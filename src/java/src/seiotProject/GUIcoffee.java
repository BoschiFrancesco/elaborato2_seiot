package seiotProject;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Enumeration;
import javax.swing.*;
import gnu.io.CommPortIdentifier;

public class GUIcoffee {
	private JFrame frame;
	private JTextField baud;
	private static SerialMonitor monitor = new SerialMonitor();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws Exception {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIcoffee window = new GUIcoffee();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 * @throws IOException 
	 * 
	 * @throws Exception
	 */
	public GUIcoffee() throws IOException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	private void initialize() throws IOException {
		frame = new JFrame("Smart coffee machine by Gomulka, Budini, Boschi");
		frame.setResizable(false);
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 13));
		Image img = new ImageIcon(this.getClass().getResource("/chicco_caffe.png")).getImage();
		frame.setIconImage(img);
		frame.getContentPane().setBackground(new Color(244, 164, 96));
		frame.setBounds(100, 100, 504, 436);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblSmartCoffeeMachine = new JLabel("Smart Coffee Machine");
		JProgressBar coffeeProgressBar = new JProgressBar();
		Choice choice = new Choice();
		JButton btnConnect = new JButton("Connect");
		JButton btnDisconnect = new JButton("Disconnect");
		JLabel stateText = new JLabel("");
		JButton btnRefillCoffee = new JButton("Refill Coffee");
		btnRefillCoffee.setEnabled(false);
		JLabel lblRefill = new JLabel("coffee level: OK");
		JLabel lblSugarLevel = new JLabel("Sugar Level");
		JSlider slider = new JSlider();
		stateText.setText("Please, connect to your coffee machine");
		monitor = new SerialMonitor();

		JPanel panel = new JPanel();
		panel.setBackground(new Color(139, 69, 19));
		panel.setBounds(0, 0, 509, 41);
		frame.getContentPane().add(panel);

		btnDisconnect.setEnabled(false);

		// Use the CommPortIdentifier and put the result to a comboBox
		Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			choice.addItem(currPortId.getName());
		}

		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				//stateText.setText("Stand by");
				monitor.start(choice.getSelectedItem().toString(), Integer.parseInt(baud.getText()), slider, stateText, btnRefillCoffee, coffeeProgressBar, lblRefill);
				btnConnect.setEnabled(false);
				btnDisconnect.setEnabled(true);
			}
		});
		
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				monitor.close();
				btnConnect.setEnabled(true);
				btnDisconnect.setEnabled(false);
			}
		});
		
		btnRefillCoffee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					monitor.Send("k");
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					System.out.println("Cannot send message to Arduino");
					e1.printStackTrace();
				}
			}
		});
		
		lblSmartCoffeeMachine.setForeground(new Color(222, 184, 135));
		lblSmartCoffeeMachine.setFont(new Font("Yu Gothic Light", Font.PLAIN, 20));
		panel.add(lblSmartCoffeeMachine);

		choice.setForeground(new Color(139, 69, 19));
		choice.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 14));

		coffeeProgressBar.setForeground(new Color(139, 69, 19));
		coffeeProgressBar.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 15));
		coffeeProgressBar.setStringPainted(true);
		coffeeProgressBar.setBounds(254, 166, 164, 33);
		frame.getContentPane().add(coffeeProgressBar);

		stateText.setForeground(new Color(102, 51, 0));
		stateText.setHorizontalAlignment(SwingConstants.CENTER);
		stateText.setFont(new Font("Yu Gothic UI Light", Font.ITALIC, 20));
		stateText.setBounds(20, 65, 458, 33);
		frame.getContentPane().add(stateText);

		btnRefillCoffee.setForeground(new Color(222, 184, 135));
		btnRefillCoffee.setBackground(new Color(139, 69, 19));
		btnRefillCoffee.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 16));
		btnRefillCoffee.setBounds(86, 220, 136, 33);
		frame.getContentPane().add(btnRefillCoffee);

		lblRefill.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 15));
		lblRefill.setBounds(254, 224, 192, 24);
		frame.getContentPane().add(lblRefill);

		lblSugarLevel.setForeground(new Color(102, 51, 0));
		lblSugarLevel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSugarLevel.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 15));
		lblSugarLevel.setBounds(118, 119, 104, 24);
		frame.getContentPane().add(lblSugarLevel);

		lblRefill.setForeground(new Color(139, 69, 19));

		slider.setEnabled(false);
		slider.setBounds(254, 109, 164, 34);
		frame.getContentPane().add(slider);
		slider.setMaximum(100);

		JLabel lblMakingCoffee = new JLabel("Making coffee...");
		lblMakingCoffee.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMakingCoffee.setForeground(new Color(102, 51, 0));
		lblMakingCoffee.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 15));
		lblMakingCoffee.setBounds(118, 168, 104, 24);
		frame.getContentPane().add(lblMakingCoffee);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(139, 69, 19));
		panel_1.setBounds(0, 292, 509, 115);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		baud = new JTextField();
		baud.setHorizontalAlignment(SwingConstants.RIGHT);
		baud.setForeground(new Color(139, 69, 19));
		baud.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 14));
		baud.setBounds(158, 56, 75, 27);
		panel_1.add(baud);
		baud.setText("9600");
		baud.setColumns(10);
		JLabel lblBaud = new JLabel("Baud Rate");
		lblBaud.setBounds(77, 54, 75, 33);
		panel_1.add(lblBaud);

		lblBaud.setForeground(new Color(244, 164, 96));
		lblBaud.setFont(new Font("Yu Gothic Light", Font.PLAIN, 15));

		choice.setBounds(158, 18, 75, 26);
		panel_1.add(choice);
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(119, 18, 33, 22);
		panel_1.add(lblPort);

		lblPort.setHorizontalAlignment(SwingConstants.LEFT);
		lblPort.setForeground(new Color(244, 164, 96));
		lblPort.setFont(new Font("Yu Gothic Light", Font.PLAIN, 15));
		btnDisconnect.setBounds(260, 55, 118, 28);
		panel_1.add(btnDisconnect);

		btnDisconnect.setForeground(new Color(139, 69, 19));
		btnDisconnect.setFont(new Font("Yu Gothic Light", Font.PLAIN, 15));
		btnDisconnect.setBackground(new Color(244, 164, 96));
		btnConnect.setBounds(260, 18, 118, 30);
		panel_1.add(btnConnect);

		btnConnect.setBackground(new Color(244, 164, 96));
		btnConnect.setForeground(new Color(139, 69, 19));
		btnConnect.setFont(new Font("Yu Gothic Light", Font.PLAIN, 15));
	}
}