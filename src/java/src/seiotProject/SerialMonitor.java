package seiotProject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

/**
 * Simple Serial Monitor, adaptation from:
 * 
 * http://playground.arduino.cc/Interfacing/Java
 *
 */
public class SerialMonitor implements SerialPortEventListener {
	SerialPort serialPort;
	String msg = "";
	JLabel state;
	JSlider sugarSlider;
	JButton refill;
	JProgressBar coffeeProg;
	JLabel lblRefill;

	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** The output stream to the port */
	public OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;

	//When the monitor starts
	public void start(String portName, int dataRate, JSlider sugar, JLabel stateText, JButton btnRefill, JProgressBar coffee, JLabel lblRef) {

		CommPortIdentifier portId = null;

		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
			// wait for arduino to reboot
			Thread.sleep(2000);
			stateText.setText("Stand By");

			// set port parameters
			serialPort.setSerialPortParams(dataRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

			state = stateText;
			sugarSlider = sugar;
			refill=btnRefill;
			sugarSlider.setMaximum(6);
			sugarSlider.setMinimum(0);
			coffeeProg=coffee;
			lblRefill=lblRef;


		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}   

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			state.setText("Please, connect to your coffee machine");
			coffeeProg.setValue(0);
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	//Send messages to Arduino
	public void Send(String s) throws InterruptedException, IOException{
		Thread.sleep(50);
		output.write(s.getBytes(Charset.forName("UTF-8")));
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	@Override
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				if (input.ready() && (msg=input.readLine()) != null){

					switch(msg) {
					case "S1": /*taking sugar value*/
						msg=input.readLine();
						sugarSlider.setValue(Integer.parseInt(msg));
						break;
					case "MT":/*needed refill, maintenance*/
						refill.setEnabled(true);
						lblRefill.setText("coffee level: REFILL");
						coffeeProg.setValue(0);
						state.setText("No more coffee. Waiting for recharge");
						break;
					case "NC":/*refilled*/
						msg=input.readLine();
						refill.setEnabled(false);
						lblRefill.setText("coffee refilled: " +msg);
						break;
					case "ON":/*machine coffee detected someone*/
						lblRefill.setText("coffee level: OK");
						state.setText("ON");
						//caffe preso
						coffeeProg.setValue(0);
						break;
					case "SB":/*Stand by mode, machine coffee sleeping*/
						state.setText("Stand By");
						break;
					case "RD":/*Welcome! machine coffee waiting for buttonPressed*/
						state.setText("Welcome! Ready");
						coffeeProg.setValue(0);
						break;
					case "WT": /*waiting user to take coffee*/
						state.setText("The coffee is ready! Waiting");
						break;
					case "WK": /*making coffee*/
						state.setText("Making a coffee");
						break;
					case "L1": /*making coffee*/
						coffeeProg.setValue(33);
						break;
					case "L2": /*making coffee*/
						coffeeProg.setValue(66);
						break;
					case "L3": /*making coffee*/
						coffeeProg.setValue(100);
						break;

					}
					msg="";
				}


			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other
		// ones.
	}

}