#ifndef __SUGARLEVELTASK__
#define __SUGARLEVELTASK__

#include "Task.h"
#include "Potenziometro.h"
#include "globalVars.h"
#include "Arduino.h"

/**
 * Class used to manage the sugar level task, which will allow user to change the amount of sugar desired.
 */
class SugarLevelTask: public Task {

  String pin;
  Potenziometro *potenziometro;
  int lastValue;

public:
  SugarLevelTask(String analogPin);  
  void init(int period);  
  void tick();
};

#endif
