#include "ButtonImpl.h"
#include "Arduino.h"

/**
 * Class used to manage the implementation of a button.
 */
ButtonImpl::ButtonImpl(int pin) {
  this->pin = pin;
  pinMode(pin, INPUT);
}

//return true if the button is pressed, false otherwise.
bool ButtonImpl::isPressed() {
  return digitalRead(pin) == HIGH;
}
