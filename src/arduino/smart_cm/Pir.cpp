#include "Pir.h"
#include <Arduino.h>

/**
 * Class used to manage the PIR, used to detect the precence of a persone
 */
Pir::Pir(int pin) {
  this->pin = pin;
}

bool Pir::movement() {
  if (digitalRead(pin) == HIGH) {
    return true;
  } else {
    return false;
  }
}
