#ifndef __BUTTON__
#define __BUTTON__

/**
 * Class used to manage button.
 */
class Button {
  public:
  //method not defined and must be implementerd in another class.
    virtual bool isPressed() = 0;
};

#endif
