#include "SugarLevelTask.h"
#include "Arduino.h"
#include "globalVars.h"

/**
 * Class used to manage the sugar level task, which will allow user to change the amount of sugar desired.
 */
SugarLevelTask::SugarLevelTask(String analogPin){
  this->pin = analogPin;    
}

//initialize the task
void SugarLevelTask::init(int period){ 
  Task::init(period);
  this->potenziometro = new Potenziometro(pin);
  this->lastValue = potenziometro->getValue();
  Serial.println("S1");  
  Serial.println(lastValue);  
}

//each tick, the actual level of sugar is read., Is it's different from the last read, comunicate the new amount to jaava interface.
void SugarLevelTask::tick(){
  //The value is read only if the system is ready (global state = READY).
	if(globalState == READY){
		int tmp = potenziometro->getValue();
		if(tmp != lastValue){
			lastValue = tmp;
			Serial.println("S1"); //aggiunto perchè in java riconosco lo zucchero con questo messaggio
			Serial.println(lastValue);
		}
	}
}
