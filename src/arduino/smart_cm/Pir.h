#ifndef __PIR__
#define __PIR__

/**
 * Class used to manage the PIR, used to detect the precence of a person
 */
class Pir {
  public:
    Pir(int pin);
    bool movement();
  protected:
    int pin;
};

#endif
