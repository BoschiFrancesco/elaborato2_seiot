/*Secondo progetto: Smart coffee machine
*Boschi Francesco matricola 0000801376
*Budini Elizabeta matrciola 0000801989
*Gomulka Konrad matricola 0000792661
*/
#include "MakingCoffeeTask.h"
#include "SugarLevelTask.h"
#include "MaintenanceTask.h"
#include "DetectDistanceTask.h"
#include "SleepTask.h"
#include "Scheduler.h"
#include "globalVars.h"

//define the number of starting coffee
#define NCOFFEE 3
//define the pin of each sensor
#define LED_1_PIN 12
#define LED_2_PIN 11
#define LED_3_PIN 10
#define BUTTON_PIN 2
#define PIR_PIN 3
#define POTENTIOMETER_PIN A0
#define ECHO_SONAR_PIN 7
#define TRIGGER_SONAR_PIN 8

//DEFINE TIME OF EACH TASK
#define WAITSCHED 50
#define WAITMAKING 100
#define SUGARTIME 100
#define MAINTENANCETIME 50
#define SLEEPTIME 100
#define DETECTDISTANCETIME 100

//Scheduler
Scheduler sched;

//These are two external variables, and this is the only file whee they are declared.
statiGlobali globalState;
int nCoffee;

//Program setup
void setup() {
  //set PinMode and Serial
  pinMode(LED_1_PIN, OUTPUT);
  pinMode(LED_2_PIN, OUTPUT);
  pinMode(LED_3_PIN, OUTPUT);
  pinMode(PIR_PIN, INPUT);
  pinMode(POTENTIOMETER_PIN, INPUT);
  pinMode(ECHO_SONAR_PIN, INPUT);
  pinMode(TRIGGER_SONAR_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(PIR_PIN, LOW);
  Serial.begin(9600);

  //Set Scheduler tick each 20ms
  sched.init(WAITSCHED);

  //Create the tasks
  Task* makingCoffee = new MakingCoffeeTask(LED_1_PIN, LED_2_PIN, LED_3_PIN);
  Task* sugarLevel = new SugarLevelTask("A0");
  Task* maintenance = new MaintenanceTask();
  Task* sleepT = new SleepTask(PIR_PIN);
  Task* detection = new DetectDistanceTask(TRIGGER_SONAR_PIN, ECHO_SONAR_PIN);
  
  //Initialize Tasks
  makingCoffee->init(WAITMAKING);
  sugarLevel->init(SUGARTIME);
  maintenance->init(MAINTENANCETIME);
  sleepT->init(SLEEPTIME);
  detection->init(DETECTDISTANCETIME);

  //Add Tasks to Scheduler
  sched.addTask(makingCoffee);
  sched.addTask(sugarLevel);
  sched.addTask(maintenance);
  sched.addTask(sleepT);
  sched.addTask(detection);

  nCoffee = NCOFFEE;
  globalState = STANDBY;
}

//program loop
void loop() {
  //Start Scheduler
  sched.schedule();
}
